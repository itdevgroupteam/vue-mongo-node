import Vue from 'vue';
import VueRouter from 'vue-router';
import Main from '@/components/Main';

Vue.use(VueRouter);
const prefix = '/Ecommerce-shop'
const routes = [
  { path: `${prefix}/`, name: 'home', component: Main },
];
const createRouter = function() {
	return new VueRouter({
	  mode: 'history',
	  routes,
	});
}
export default createRouter;

