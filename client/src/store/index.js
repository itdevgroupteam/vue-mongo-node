import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';
import json from '@/store/products.json'

Vue.use(Vuex);

function createStore () {
  return  new Vuex.Store({
    /**
     * @param  {type: Number 0} totalItems
     * @param  {type: Array  []} refunds
     * @param  {type: String  'http:orders:[]} apiHost
     */
    state: () => ({
        basket: [],
        totalItems: 0,
        refunds: [],
        apiHost: process.env.VUE_APP_API_HOST || 'http://localhost:3000',
        pagination: {
          products: {
            pages: 0,
            visiblePages: 0,
            rightMax: false,
            leftMax: false,
            first: 1,
            active: 1,
            dirty: true,
          }, 
        }
    }),
  mutations: {
    setPagination(state, {type, val}){
      state.pagination[type] = val
    },
    setRefunds(state, products) {
      state.refunds = products;
    },
  },
  actions: {
    actionRefunds({ commit, state }) {
        const url = `${state.apiHost}/getRefunds`;
        return axios.get(url).then((response) => {
          const refunds = response.data;
          commit('setRefunds', refunds);
        }).catch(err => {
          console.log("Couldn't get refunds")
          commit('setRefunds', json);
        });
    },
  },
});
}
export default createStore;