import Vue from 'vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import 'vuetify/dist/vuetify.css'

Vue.use(Vuetify, {
    iconfont: 'md',
})

const opts = {
}

export default new Vuetify(opts)