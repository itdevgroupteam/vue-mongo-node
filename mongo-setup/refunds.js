// import refunds from './refunds-mokups';

const refunds = [
    {
        date: "2021-03-15T20:18:23.000Z",
        subject: "Your refund for Moon and Back by Hanna Andersson....",
        amount: {
            text: "$29.42",
            number: "29.42"
        },
        productName: "Moon and Back by Hanna Andersson...",
        merchant: "Amazon",
        status: "completed"
    },
    {
        date: "2021-03-09T17:37:38.000Z",
        subject: "Your return of Moon and Back by Hanna Andersson...",
        amount: {
            text: "$29.42",
            number: "29.42"
        },
        productName: "Moon and Back by Hanna Andersson...",
        status: "started",
        merchant: "Amazon",
        link:
            "https://www.amazon.com/spr/returns/cart?orderId=111-0947679-7589059&ref_=E_RRT_status"
    },
    {
        "date": "2021-03-01T22:59:38.000Z",
        "subject": "Your refund for Rubie's DC Comics Superman....",
        "amount": {
            "text": "$29.35",
            "number": "29.35"
        },
        "productName": "Rubie's DC Comics Superman...",
        "merchant": "Amazon",
        "status": "completed"
    },
    {
        "date": "2021-03-01T17:02:32.000Z",
        "subject": "Your return of Rubie's DC Comics Superman... and 1 other item.",
        "amount": {
            "text": "$41.33",
            "number": "41.33"
        },
        "status": "started",
        "merchant": "Amazon",
        "link": "https://www.amazon.com/gp/css/order-history/?ref_=E_YourOrder_Page"
    },
    {
        "date": "2021-02-24T04:59:58.000Z",
        "subject": "Your refund for GRECERELLE Women's Casual Loose....",
        "amount": {
            "text": "$31.60",
            "number": "31.60"
        },
        "productName": "GRECERELLE Women's Casual Loose...",
        "merchant": "Amazon",
        "status": "completed"
    },
    {
        "date": "2021-02-24T03:22:10.000Z",
        "subject": "Your refund for Lveberw Long Nightgown....",
        "amount": {
            "text": "$21.78",
            "number": "21.78"
        },
        "productName": "Lveberw Long Nightgown...",
        "merchant": "Amazon",
        "status": "completed"
    },
    {
        "date": "2021-02-24T02:26:28.000Z",
        "subject": "Your refund for LEGO DC Batman: Batman vs The....",
        "amount": {
            "text": "$42.20",
            "number": "42.20"
        },
        "productName": "LEGO DC Batman: Batman vs The...",
        "merchant": "Amazon",
        "status": "completed"
    },
    {
        "date": "2021-02-24T02:24:55.000Z",
        "subject": "Your refund for GRECERELLE Women's Casual Loose....",
        "amount": {
            "text": "$31.60",
            "number": "31.60"
        },
        "productName": "GRECERELLE Women's Casual Loose...",
        "merchant": "Amazon",
        "status": "completed"
    }
]

console.log('REFUNDS', refunds)
const RefundModel = require('./models/entities/Refunds')

const getContent = new Promise((resolve, reject) => {
  RefundModel.find().then(res => resolve(res), err => { console.log(err); reject(err)})
})

const saveContent = new Promise((resolve, reject) => {
    if (refunds.length) {
      RefundModel.insertMany(refunds)
         .then(res => {
           resolve(res)
         })
         .catch(err => {
          reject(err)
      })
    } else { reject('No refunds'); }
})

exports.getContent = getContent;
exports.saveContent = saveContent;
