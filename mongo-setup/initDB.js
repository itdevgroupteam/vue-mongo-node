const mongoose = require('mongoose')
const importRefunds = require('./refunds');
const server = 'mongodb://mongo:27017'
const database = 'testdb';

mongoose.connect(`${server}/${database}`, {useNewUrlParser: true, useUnifiedTopology: true}).then(() => {
  /*** Import refunds to MongoDB ***/
  importRefunds.saveContent.then(res => {
    console.log('Saved refunds')
  }, err => console.log(err))
})
