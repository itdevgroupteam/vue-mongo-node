const mongoose = require('mongoose');
const Schema = mongoose.Schema;
/**
 * @param  {type:String} subject
 * @param  {type:Object} amount
 * @param  {type:String} productName
 * @param  {type:String} merchant
 * @param  {type:String} satus
 * @param  {type:String} link
 * @param  {type:Date} timestamps
 */
const refundSchema = new Schema({
  subject: {
    type: String,
    required: true
  },
  amount: {
    type: Object,
    required: true
  },
  productName: {
    type: String,
    required: false
  },
  merchant: {
    type: String,
    required: true
  },
  status: {
    type: String,
    required: true
  },
  link: {
    type: String,
    required: false
  },
}, { timestamps: true })

const Refund = mongoose.model('Refund', refundSchema);
module.exports = Refund;
